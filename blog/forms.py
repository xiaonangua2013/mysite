from .models import Comment
from django import forms
from django_summernote.widgets import SummernoteWidget, SummernoteInplaceWidget
from django_summernote.fields import SummernoteTextFormField, SummernoteTextField


class CommentForm(forms.ModelForm):
    body = forms.CharField(widget=SummernoteWidget())
    # body = SummernoteTextField()

    class Meta:
        model = Comment
        fields = ('name', 'email', 'body')
        widgets = {
            'body': SummernoteWidget(),
            # 'bar': SummernoteInplaceWidget(),
        }

from .models import Image
class ImageForm(forms.ModelForm):
    """Form for the image model"""
    class Meta:
        model = Image
        fields = ('title', 'image')
